<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="">
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/app.css">
<link rel='stylesheet' href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.4.1/mapbox-gl.css' type='text/css'/>
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.css' type='text/css'/>

<title>Sitemotion.</title>
<Style>
html {height: 100%;} body { height: 100%; margin: 0; padding: 0;} 
</style>
</head>
<body>
    <div id="app">
        <app-component></app-component>
    </div>

    <script src="js/app.js"></script>
</body>
</html>
