import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

const state = {
    isLoggedIn: !!localStorage.getItem('token'),
    user: {},
    project: [],
}

const getters = {
    getUser(state) {
        return state.user
    },
    getProject(state) {
        return state.project
    }

}

const actions = {

}

const mutations = {
    loginUser(state) {
        state.isLoggedIn = true
    },
    logoutUser(state) {
        state.isLoggedIn = false
    },
    setUser(state, user) {
        state.user = user
    },
    setProject(state, project) {
        state.project = project
    }


}

export default new Vuex.Store({
    state,
    getters,
    mutations,
    actions
})
