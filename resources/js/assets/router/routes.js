import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/store'

Vue.use(VueRouter)

import DashboardComponent from '../components/DashboardComponent'
import LoginComponent from '../components/LoginComponent'
import LogoutComponent from '../components/LogoutComponent'
import OrderFormComponent from '../components/OrderFormComponent'
import ProjectDetailsComponent from '../components/ProjectDetailsComponent'
import ProjectSettingsComponent from '../components/subcomponents/ProjectSettingsComponent'
import OrderListComponent from '../components/subcomponents/OrderListComponent'
import DocumentComponent from '../components/subcomponents/DocumentComponent'


const routes = [
	{
		path: '/',
		redirect: { name: 'login' },
		meta: {
			hideSidebar: true,
			breadcrumb: {
				label: 'Login'
			}
		}
	},
	{
		name: 'dashboard',
		path: '/dashboard',
		component: DashboardComponent,
		meta: {

			requiresAuth: true, // redirects to login if auth is not set.
			breadcrumb: {
			   label: 'Dashboard'
			}
		}

	},
	{
		name: 'projectdetails/:pid/:uid',
		path: '/projectdetails/:pid/:uid',
		component: ProjectDetailsComponent,
		meta: {
			props: true,
			requiresAuth: true, // redirects to login if auth is not set.
			breadcrumb: {
				label: 'Project',
				Parent: 'dashboard'
			}

		}
	},
	{
		name: 'psettings',
		path: '/settings',
		component: ProjectSettingsComponent,
		meta: {
			props: true,
			requiresAuth: true, // redirects to login if auth is not set.
			breadcrumb: {
				label: 'Settings',
				Parent: 'dashboard'
			}

		}
	},
	{
		name: 'orderform',
		path: '/orderform',
		component: OrderFormComponent,
		meta: {
			props: true,
			requiresAuth: true, // redirects to login if auth is not set.
			breadcrumb: {
				label: 'orderform',
				Parent: 'dashboard'
			}

		}
	},
	{
		name: 'olist',
		path: '/orderlist',
		component: OrderListComponent,
		meta: {
			props: true,
			requiresAuth: true, // redirects to login if auth is not set.
			breadcrumb: {
				label: 'Order List',
				Parent: 'dashboard'
			}

		}
	},
	{
		name: 'document',
		path: '/document',
		component: DocumentComponent,
		meta: {
			props: true,
			requiresAuth: true, // redirects to login if auth is not set.
			breadcrumb: {
				label: 'Documents',
				Parent: 'dashboard'
			}

		}
	},
	{
		name: 'login',
		path: '/login',
		component: LoginComponent,
		meta: {
			hideSidebar: true,
			breadcrumb: {
				label: 'Login'
			}
		}
	},
	{
		name: 'logout',
		path: '/logout',
		component: LogoutComponent
	},
]

const router = new VueRouter({
	mode: 'history',
	routes

})

router.beforeEach((to, from, next) => {

	// check if the route requires authentication and user is not logged in
	if (to.matched.some(route => route.meta.requiresAuth) && !store.state.isLoggedIn) {
		// redirect to login page
		next({ name: 'login' })
		return
	}

	// if logged in redirect to dashboard
	if (to.path === '/login' && store.state.isLoggedIn) {
		next({ name: 'dashboard' })
		return
	}

	next()
})

export default router
