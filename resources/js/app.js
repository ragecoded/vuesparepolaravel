require('./bootstrap');

import Vue from 'vue'
import Vuex from 'vuex'
import Vue2Crumbs from 'vue-2-crumbs'
import BootstrapVue from 'bootstrap-vue'



import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(Vue2Crumbs)
Vue.use(Vuex)

import router from './assets/router/routes.js';
import AppComponent from './assets/components/AppComponent'
import store from './assets/store/store'
import axios from 'axios'

/*
Helper vue components import
*/
import Sidebar from './assets/components/helpers/sidebar'
import Navbar from './assets/components/helpers/navbar'
import Modal from './assets/components/helpers/modal'
import Searchbar from './assets/components/helpers/Searchbar'

/*
Component references
*/
Vue.component('Sidebar', Sidebar)
Vue.component('Navbar', Navbar)
Vue.component('Modal', Modal)
Vue.component('Searchbar', Searchbar)



const app = new Vue({
    components: { AppComponent },
    store,
    router
}).$mount('#app')
