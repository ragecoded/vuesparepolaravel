<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'api','prefix' => 'auth'], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::middleware('auth:api')->group(function () {
    Route::get('dashboard', function () {
        return response()->json(['data' => 'Test Data']);
    });
});

Route::namespace('Api')->group(function () {
    Route::get('/projects', 'ProjectsController@index');
    Route::post('/projects', 'ProjectsController@store');
    Route::get('/projects/{uID}', 'ProjectsController@userProjects');
    Route::get('/projects/{uID}/{pID}', 'ProjectsController@projectDetails');
    Route::get('/services', 'ServiceController@serviceOutput');
    Route::get('/subservices', 'SubServiceController@subServiceOutput');
    Route::get('/subservices/{pID}', 'SubServiceController@parentSubServiceOutput');
    Route::post('/orderinput', 'OrderController@store');
});
