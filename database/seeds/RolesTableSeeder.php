<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'client',
            'display_name' => 'Normal user',
            'description' => 'A user with normal access rights. Can create and manage projects.',
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => 'Admin user',
            'description' => 'A user full access rights. This user has access to all backend functions.',
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		DB::table('roles')->insert([
            'name' => 'provider_admin',
            'display_name' => 'Provider admin',
            'description' => 'Provider user with admin rights. Can create new users on behalf of his organization.',
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'name' => 'provider',
            'display_name' => 'Provider user',
            'description' => 'Provider user with normal access rights.',
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
