<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert([
            'title' => 'Nordliggende helikopter plads',
			'user_id' => 5,
			'project_type_id' => 1,
			'long' => 55.3844130,
			'lat' => 10.3647170,
			'project_address' => 'J. B. Winsløws Vej',
			'project_house_number' => '4',
			'project_postal_code' => 5000,
			'project_city' => 'Odense',
			'project_country' => 'DK',
			'completed' => false,
			'approved' => false,
			'is_owner' => true,
			'owner_name' => "",
			'owner_email' => "",
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		DB::table('projects')->insert([
            'title' => 'Randers parkeringshus',
			'user_id' => 5,
			'project_type_id' => 1,
			'long' => 56.4579420,
			'lat' => 10.0408570,
			'project_address' => 'Viborgvej',
			'project_house_number' => '80',
			'project_postal_code' => 8920,
			'project_city' => 'Randers',
			'project_country' => 'DK',
			'completed' => false,
			'approved' => false,
			'is_owner' => true,
			'owner_name' => "",
			'owner_email' => "",
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
