<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$json = Storage::disk('local')->get('public/resources/zipcodes.json');
		$array = json_decode($json, true);
		foreach($array as $data)
		{
			DB::table('cities')->insert([
				'name' => $data["navn"],
				'postal_code' => $data["nr"],
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'created_at' => Carbon::now()->format('Y-m-d H:i:s')
			]);
		}
    }
}
