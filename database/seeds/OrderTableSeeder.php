<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            'project_id' => 1,
            'provider_id' => null,
            'service_id' => 1,
            'drone_pilot_id' => null,
            'coordinates' =>
				'{
				  "type": "FeatureCollection",
				  "features": [
					{
					  "type": "Feature",
					  "properties": {},
					  "geometry": {
						"type": "Polygon",
						"coordinates": [
						  [
							[
							  10.364519655704498,
							  55.38401703876844
							],
							[
							  10.364535748958586,
							  55.38421207079147
							],
							[
							  10.363151729106903,
							  55.38427149441983
							],
							[
							  10.36310613155365,
							  55.38409017588977
							],
							[
							  10.364519655704498,
							  55.38401703876844
							]
						  ]
						]
					  }
					}
				  ]
				}',
            'cadastre' => null,
            'scan_time' => Carbon::now()->addMonths(1)->format('Y-m-d H:i:s'),
            'provider_accept_time' => false,
            'client_accept_time' => true,
            'order_status' => 1,
			'sqr_meter' => null,
            'minutes_spent_in_air' => null,
            'events' => '',
			'gsd' => null,
            'max_height' => null,
			'total_price' => 0,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

		DB::table('orders')->insert([
            'project_id' => 2,
            'provider_id' => null,
            'service_id' => 3,
            'drone_pilot_id' => null,
            'coordinates' =>
				'{
				  "type": "FeatureCollection",
				  "features": [
					{
					  "type": "Feature",
					  "properties": {},
					  "geometry": {
						"type": "Polygon",
						"coordinates": [
						  [
							[
							  10.047222375869751,
							  56.46119233981711
							],
							[
							  10.048187971115112,
							  56.46157171071034
							],
							[
							  10.04772663116455,
							  56.46188587436277
							],
							[
							  10.04673957824707,
							  56.46154800014052
							],
							[
							  10.047222375869751,
							  56.46119233981711
							]
						  ]
						]
					  }
					}
				  ]
				}',
            'cadastre' => null,
            'scan_time' => Carbon::now()->addMonths(2)->format('Y-m-d H:i:s'),
            'provider_accept_time' => false,
            'client_accept_time' => true,
            'order_status' => 1,
			'sqr_meter' => null,
            'minutes_spent_in_air' => null,
            'events' => '',
			'gsd' => null,
            'max_height' => null,
			'total_price' => 0,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
