<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProviderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Generates provider "BuildCode" (actual provider)
        DB::table('providers')->insert([
            'email' => 'mhn@buildcode.dk',
			'provider_name' => 'Buildcode',
            'provider_address_road' => 'Svendborgvej',
			'provider_house_number' => '226',
            'provider_postal_code' => 5260,
            'provider_city' => 'Odense',
            'provider_country_code' => 'DK',
            'provider_description' => null,
            'cvr' => 38355058,
            'bank_account' => null,
            'provider_website_url' => 'https://buildcode.dk',
            'provider_img_url' => null,
            'total_assignments_completed' => 87,
            'total_sqr_meter_scanned' => 980.7,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		// Generates provider "MagnumDrones" (non-existing)
		DB::table('providers')->insert([
            'email' => 'mgdrones@mail.dk',
			'provider_name' => 'MagnumDrones',
            'provider_address_road' => 'Skovbrynet',
			'provider_house_number' => '4',
            'provider_postal_code' => 6000,
            'provider_city' => 'Kolding',
            'provider_country_code' => 'DK',
            'provider_description' => null,
            'cvr' => 29174437,
            'bank_account' => null,
            'provider_website_url' => null,
            'provider_img_url' => null,
            'total_assignments_completed' => 21,
            'total_sqr_meter_scanned' => 451.5,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		// Generates provider "3D-metrics" (non-existing)
		DB::table('providers')->insert([
            'email' => '3dmetrics@mail.dk',
			'provider_name' => '3D-metrics',
            'provider_address_road' => 'Munkesøvej',
			'provider_house_number' => '17',
            'provider_postal_code' => 4000,
            'provider_city' => 'Roskilde',
            'provider_country_code' => 'DK',
            'provider_description' => null,
            'cvr' => 19882345,
            'bank_account' => null,
            'provider_website_url' => null,
            'provider_img_url' => null,
            'total_assignments_completed' => 38,
            'total_sqr_meter_scanned' => 630.3,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
