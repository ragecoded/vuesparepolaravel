<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
            'package_title' => '12 Flyvninger',
            'package_description' => 'Doner cillum ham leberkas est strip steak occaecat pork belly short loin spare ribs landjaeger elit pork loin veniam. Veniam doner biltong culpa sausage adipisicing andouille chicken. Mollit elit do, cupim jowl pork in sirloin rump duis nostrud exercitation esse est picanha. Corned beef nostrud beef strip steak, duis pariatur ut veniam labore. Tempor chuck sint pancetta, meatball eu bresaola magna fatback.',
            'scans_included' => 12,
            'package_price' => 31000,
            'package_img_url' => '',
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('packages')->insert([
            'package_title' => '36 Flyvninger',
            'package_description' => 'Dolore drumstick aute beef ribs, rump aliquip shoulder tenderloin pork belly. Leberkas id pastrami, in ham lorem hamburger short ribs. Pariatur tail consequat proident meatball turkey ground round pig alcatra. In lorem prosciutto ball tip, porchetta eiusmod irure buffalo sint pork belly beef shoulder sed shankle mollit.',
            'scans_included' => 36,
            'package_price' => 58000,
            'package_img_url' => '',
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
