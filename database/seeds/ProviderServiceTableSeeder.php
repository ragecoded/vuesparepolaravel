<?php

use Illuminate\Database\Seeder;

class ProviderServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// Buildcode
        DB::table('provider_service')->insert([
            'provider_id' => 1,
			'service_id' => 1
        ]);
		DB::table('provider_service')->insert([
            'provider_id' => 1,
			'service_id' => 2
        ]);
		DB::table('provider_service')->insert([
            'provider_id' => 1,
			'service_id' => 3
        ]);
		// MagnumDrones
		DB::table('provider_service')->insert([
            'provider_id' => 2,
			'service_id' => 1
        ]);
		DB::table('provider_service')->insert([
            'provider_id' => 2,
			'service_id' => 2
        ]);
		// 3Dmetrics
		DB::table('provider_service')->insert([
            'provider_id' => 3,
			'service_id' => 2
        ]);
		DB::table('provider_service')->insert([
            'provider_id' => 3,
			'service_id' => 3
        ]);
		DB::table('provider_service')->insert([
            'provider_id' => 3,
			'service_id' => 4
        ]);
    }
}
