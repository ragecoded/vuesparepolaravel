<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$this->call(UsersTableSeeder::class);
		$this->call(ProviderTableSeeder::class);  
		$this->call(ProviderUsersTableSeeder::class);
        $this->call(DroneOperationsTableSeeder::class);
        $this->call(ProjectTypesTableSeeder::class);
        $this->call(DronesTableSeeder::class);
        $this->call(PackageTableSeeder::class);
        $this->call(PriceTableSeeder::class);
		$this->call(ProjectTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);
		$this->call(CitiesTableSeeder::class);
		$this->call(ServicesTableSeeder::class);
		$this->call(ProviderServiceSeeder::class);
		$this->call(OrderTableSeeder::class);
    }
}
