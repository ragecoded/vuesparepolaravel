<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProviderUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Expands on user "mhn@buildcode.dk"
        DB::table('provider_users')->insert([
            'user_id' => 1,
			'provider_id' => 1,
            'google_calendar_id' => '2374728',
			'microsoft_calendar_id' => null,
            'price_per_hour' => 350,
            'total_assigned_tasks' => 35,
            'total_completed_scans' => 105,
            'total_sqr_meter_scanned' => 456.8,
            'role' => "",
            'has_drone_certificate' => true,
            'drone_certificate' => 38355058,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		// Expands on user "andre@buildcode.dk"
        DB::table('provider_users')->insert([
            'user_id' => 2,
			'provider_id' => 1,
            'google_calendar_id' => '3928174',
			'microsoft_calendar_id' => null,
            'price_per_hour' => 250,
            'total_assigned_tasks' => 4,
            'total_completed_scans' => 20,
            'total_sqr_meter_scanned' => 123.0,
            'role' => "",
            'has_drone_certificate' => false,
            'drone_certificate' => null,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		// Expands on user "mgdrones@mail.dk"
        DB::table('provider_users')->insert([
            'user_id' => 3,
			'provider_id' => 2,
            'google_calendar_id' => '2381945',
			'microsoft_calendar_id' => null,
            'price_per_hour' => 250,
            'total_assigned_tasks' => 3,
            'total_completed_scans' => 13,
            'total_sqr_meter_scanned' => 45.7,
            'role' => "",
            'has_drone_certificate' => true,
            'drone_certificate' => 38386746,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		// Expands on user "3dmetrics@mail.dk"
        DB::table('provider_users')->insert([
            'user_id' => 4,
			'provider_id' => 3,
            'google_calendar_id' => '2810470',
			'microsoft_calendar_id' => null,
            'price_per_hour' => 250,
            'total_assigned_tasks' => 17,
            'total_completed_scans' => 36,
            'total_sqr_meter_scanned' => 209.8,
            'role' => "",
            'has_drone_certificate' => true,
            'drone_certificate' => 29384457,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
