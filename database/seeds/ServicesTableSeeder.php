<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            'service_name' => 'Service 1',
            'service_description' => 'This is a description of the service.',
			'service_img_url' => null,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		DB::table('services')->insert([
            'service_name' => 'Service 2',
            'service_description' => 'This is a description of the service.',
			'service_img_url' => null,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		DB::table('services')->insert([
            'service_name' => 'Service 3',
            'service_description' => 'This is a description of the service.',
			'service_img_url' => null,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);DB::table('services')->insert([
            'service_name' => 'Service 4',
            'service_description' => 'This is a description of the service.',
			'service_img_url' => null,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
