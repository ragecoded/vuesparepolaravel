<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	// Provider accounts
        DB::table('users')->insert([
            'firstname' => 'Martin',
            'lastname' => 'Niminski',
			'bio' => '',
            'email' => 'mhn@buildcode.dk',
            'password' => bcrypt('admin'),
            'phone' => '004542785456',
            'address' => "Svendborgvej",
			'house_number' => '226',
            'postal_code' => 5260,
            'city' => 'Odense',
            'country_code' => 'DK',
            'regristration_completed' => true,
            'accept_terms_and_conditions' => true,
            'accept_policies' => true,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
            'firstname' => 'André',
            'lastname' => 'Gollubits',
			'bio' => 'Full-stack software developer',
            'email' => 'andre@buildcode.dk',
            'password' => bcrypt('12345'),
            'phone' => '004541832388',
            'address' => "Bredstedgade",
			'house_number' => '1 1 105',
            'postal_code' => 5000,
            'city' => 'Odense C',
            'country_code' => 'DK',
            'regristration_completed' => true,
            'accept_terms_and_conditions' => true,
            'accept_policies' => true,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		DB::table('users')->insert([
            'firstname' => 'Henrik',
            'lastname' => 'Machnumsen',
			'bio' => '',
            'email' => 'mgdrones@mail.dk',
            'password' => bcrypt('12345'),
            'phone' => '004523894949',
            'address' => null,
			'house_number' => null,
            'postal_code' => null,
            'city' => null,
            'country_code' => null,
            'regristration_completed' => true,
            'accept_terms_and_conditions' => true,
            'accept_policies' => true,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		DB::table('users')->insert([
            'firstname' => 'Niels',
            'lastname' => 'Børgensen',
			'bio' => '',
            'email' => '3dmetrics@mail.dk',
            'password' => bcrypt('12345'),
            'phone' => '004530304567',
            'address' => null,
			'house_number' => null,
            'postal_code' => null,
            'city' => null,
            'country_code' => null,
            'regristration_completed' => true,
            'accept_terms_and_conditions' => true,
            'accept_policies' => true,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
		// Client account
		DB::table('users')->insert([
            'firstname' => 'Søren',
            'lastname' => 'Andersen',
			'bio' => '',
            'email' => 'soeren@mail.dk',
            'password' => bcrypt('12345'),
            'phone' => '004510102543',
            'address' => null,
			'house_number' => null,
            'postal_code' => null,
            'city' => null,
            'country_code' => null,
            'regristration_completed' => true,
            'accept_terms_and_conditions' => true,
            'accept_policies' => true,
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
