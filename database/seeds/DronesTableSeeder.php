<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DronesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('drones')->insert([
            'provider_id' => 1,
            'drone_name' => 'Matrice 600',
            'drone_model' => 'A3',
            'drone_id' => 11063,
            'drone_register_number' => 11063,
            'drone_sensor' => 'X3, X5, A7R II',
			'drone_visual_line_of_sight' => 800,
            'control_station' => 2,
            'description' => 'Matrice 600 drone.',
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('drones')->insert([
            'provider_id' => 1,
            'drone_name' => 'Mavic Air',
            'drone_model' => 'DJI',
            'drone_id' => 34930,
            'drone_register_number' => 12043,
            'drone_sensor' => 'N/A',
			'drone_visual_line_of_sight' => 200,
            'control_station' => 3,
            'description' => 'Mavic Air drone.',
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('drones')->insert([
            'provider_id' => 1,
            'drone_name' => 'Phantom 4 Advanced',
            'drone_model' => 'Advanced',
            'drone_id' => 13663,
            'drone_register_number' => 13663,
            'drone_sensor' => 'N/A',
			'drone_visual_line_of_sight' => 400,
            'control_station' => 4,
            'description' => 'Phantom 4 Advanced drone. An uprated camera is equipped with a 1-inch 20-megapixel sensor capable of shooting 4K/60 fps video and Burst Mode stills at 14 fps.',
			'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
