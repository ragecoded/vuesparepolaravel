<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('project_type_id')->unsigned()->nullable();
            $table->foreign('project_type_id')->references('id')->on('project_types')->onUpdate('cascade')->onDelete('cascade');
			$table->decimal('lat', 10, 7)->nullable();
			$table->decimal('long', 10, 7)->nullable();
            $table->string('project_address', 50);
			$table->string('project_house_number');
            $table->integer('project_postal_code');
            $table->string('project_city', 50);
			$table->string('project_country', 2);
			$table->string('thumbnail_url')->nullable()->default(null);
            $table->boolean('completed')->default(false);
			$table->boolean('approved')->default(false);
			$table->boolean('is_owner')->default(false);
			$table->string('owner_name')->nullable();
			$table->string('owner_email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
