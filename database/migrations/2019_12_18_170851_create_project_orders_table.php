<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Project_Orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('p_id');
            $table->date('date');
            $table->string('pname', 255)->nullable();
			$table->integer('ownerstatus')->default(0);
			$table->string('ownername', 255)->nullable();
			$table->string('owneremail', 255)->nullable();
            $table->integer('area')->default(0);
			$table->integer('perimeter')->default(0);
			$table->integer('package')->default(0);
            $table->integer('service_id')->unsigned()->nullable(); //serviceselect
            $table->integer('sub_service_id')->unsigned()->nullable(); // subserviceselect
            $table->foreign('p_id')->references('id')->on('projects')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('sub_service_id')->references('id')->on('sub_services')->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('package')->references('id')->on('packages')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }
			/**
				formdata.date
				formdata.serviceselect
				formdata.subserviceselect
				formdata.area
				formdata.perim
				formdata.package
				formdata.pname
				formdata.ownerstatus
				formdata.ownername
				formdata.owneremail
			**/
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Project_Orders');
    }
}
