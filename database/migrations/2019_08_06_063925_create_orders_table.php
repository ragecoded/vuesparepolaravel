<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned()->nullable();
            $table->foreign('project_id')->references('id')->on('projects')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('provider_id')->unsigned()->nullable();
            $table->foreign('provider_id')->references('id')->on('providers')->onUpdate('cascade')->onDelete('cascade');
			$table->integer('order_service_id')->unsigned();
            $table->foreign('order_service_id')->references('id')->on('order_services')->onUpdate('cascade')->onDelete('cascade');
            $table->text('gps_coordinates');
            $table->string('cadastre', 7)->nullable();
            $table->timestamp('scan_time');
            $table->float('sqr_meter')->nullable();
            $table->integer('minutes_spent_in_air')->nullable();
            $table->text('events');
            $table->float('gsd')->nullable();
            $table->integer('max_height')->nullable();
			$table->integer('total_price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
