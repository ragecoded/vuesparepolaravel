<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('provider_name', 100);
            $table->string('provider_address_road', 100);
			$table->string('provider_house_number', 50);
            $table->integer('provider_postal_code');
            $table->string('provider_city', 50);
            $table->string('provider_country_code', 2);
			$table->string('provider_description', 255)->nullable();
            $table->integer('cvr');
            $table->integer('bank_account')->nullable();
            $table->integer('total_assignments_completed')->default(0);
            $table->float('total_sqr_meter_scanned', 8, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operators');
    }
}
