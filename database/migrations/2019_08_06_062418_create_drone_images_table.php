<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDroneImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drone_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('drone_id')->unsigned();
            $table->foreign('drone_id')->references('id')->on('drones')->onUpdate('cascade')->onDelete('cascade');
            $table->string('drone_image_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drones_images');
    }
}
