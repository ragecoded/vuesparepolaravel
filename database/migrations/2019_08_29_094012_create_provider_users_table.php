<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_users', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
			$table->integer('provider_id')->unsigned();
            $table->foreign('provider_id')->references('id')->on('providers')->onUpdate('cascade')->onDelete('cascade');
			$table->string('google_calendar_id')->nullable();
			$table->string('microsoft_calendar_id')->nullable();
			$table->integer('price_per_hour')->default(0);
			$table->integer('total_assigned_tasks')->default(0);
			$table->integer('total_completed_scans')->default(0);
			$table->float('total_sqr_meter_scanned', 8, 2)->default(0);
			$table->string('drone_certificate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_users');
    }
}
