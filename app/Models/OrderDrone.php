<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderDrone extends Pivot
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'drone_id'
    ];

    // Name explanations:
    // order_id = Foreign key for model:Order
	// drone_id = Foreign key for model:Drone
}
