<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'postal_code', 'name'
    ];

    // Name explanations:
    // postal_code = The postal code corresponding to the city
    // city_name = The city name/area corresponding to the city
}
