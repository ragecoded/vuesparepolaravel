<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DroneOperation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    // Name explanations:
    // name = Specifies which operation it is (VLOS, BVLOS, AUTONOM)
}
