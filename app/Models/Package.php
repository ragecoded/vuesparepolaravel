<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'package_title', 'package_description', 'scans_included', 'package_price', 'package_img_url'
    ];

    // Name explanations:
    // package_title = The title for the package
    // package_description = The description for the package
    // scans_included = The amount of drone scans in this package
    // package_price = The price in DKK for the package
    // package_img_url = The url of the attached image (if uploaded)
}
