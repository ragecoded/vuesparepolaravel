<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'base_price_per_sqr_meter', 'base_price_per_sqr_meter_facade', 'base_price_per_floor'
    ];

    // Name explanations:
    // base_price_per_sqr_meter = The base price per square meter of horizontal flat ground
    // base_price_per_sqr_meter_facade = The base price per square meter of facade (walls, windows, exterior poles etc.)
    // base_price_per_floor = The base price per floor in the building
}
