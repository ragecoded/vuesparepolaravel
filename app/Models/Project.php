<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_name', 'user_id', 'project_type_id', 'project_address', 'project_house_number', 'project_postal_code', 'project_city', 'project_country', 'lat', 'long',
		'completed', 'approved', 'is_owner', 'owner_name', 'owner_email'
    ];

    // Name explanations:
    // project_name = The name of the project
    // user_id = Foreign key for model:User
    // project_type_id = Foreign key for model:ProjectType
    // project_address = The road name of the project
	// project_house_number = The house number (stored seperate for practical reasons)
    // project_postal_code = The postal/zip code of the project
    // project_city = The city of the project
    // project_country = The country of the project
    // lat = Latitude. GPS Y coordinate
    // long = Longitude, GPS X cooordinate
	// thumbnail_url = The URL to the thumbnail image
	// completed = Boolean which defines whether the project is completed or not
	// approved = Boolean which defines whether the owner has approved the drone to use the airspace around the building
	// is_owner = Boolean which defines whether the creator of the project is the owner of the building/area specified in the project
	// owner_name = The name of the owner
	// owner_email = The email of the owner

	/**
     * Retrieves the owner of the project.
     */
    public function owner()
    {
		// Looks for id on user table and compares with the local user_id
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

	/**
     * Retrieves the type of the project.
     */
    public function type()
    {
        return $this->hasOne('App\Models\ProjectType', 'id', 'project_type_id');
    }

	/**
     * Retrieves providers attached to the project.
     */
	public function providers()
    {
		// Projects belongsToMany providers using the pivot table "ProjectProvider"
        return $this->belongsToMany('App\Models\Provider')->using('App\Models\ProjectProvider');
    }
}
