<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderService extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_name', 'service_description', 'service_img_url'
    ];

    // Name explanations:
    // service_name = Name of the service
	// service_description = Optional description of the service
	// service_img_url = Optional image of the service
}
