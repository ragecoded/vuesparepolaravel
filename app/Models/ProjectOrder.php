<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectOrder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'p_id','date', 'service_id', 'sub_service_id', 'area', 'perimiter', 'package', 'pname', 'created_at', 'updated_at'
    ];

    // Name explanations:
    // name = A string which defines the project type (examples: construction site, rooftop, building)
}

