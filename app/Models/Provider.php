<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'provider_name', 'provider_address_road', 'provider_house_number', 'provider_postal_code', 'provider_city', 'provider_country_code',
		'provider_description', 'cvr', 'bank_account', 'total_assignments_completed', 'total_sqr_meter_scanned'
    ];

    // Name explanations:
	// provider_* = Self-explaining
	// provider_house_number = We store the house number in a seperate column for practical reasons (some API's require house numbers seperated)
    // cvr = CVR number
    // bank_account = Unused for now. Can be changed in future version
    // total_assignments_completed = Used for statistics.
    // total_sqr_meter_scanned = Used for statistics.

	/**
     * Returns all the users belonging to the specified provider.
     *
     * @var array
     */
	public function users()
	{
		return $this->belongsTo('App\Models\ProviderUser', 'id');
	}

	/**
     * Returns all the projects attached to the provider.
     *
     * @var array
     */
	public function projects()
	{
		// Provider belongsToMany projects using the pivot table "ProjectProvider"
        return $this->belongsToMany('App\Models\Project')->using('App\Models\ProjectProvider');
	}

}
