<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DroneImage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'drone_id', 'drone_image_url'
    ];

    // Name explanations:
    // drone_id = Foreign key for model:Drone
    // drone_img_url = The URL path for the images attached to this drone
}
