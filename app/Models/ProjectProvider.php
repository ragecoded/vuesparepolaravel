<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProjectProvider extends Pivot
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id', 'provider_id'
    ];

	// Name explanations:
    // project_id = Foreign key for model:Project
	// provider_id = Foreign key for model:Provider

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'project_provider';


}
