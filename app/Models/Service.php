<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'service_name', 'service_description', 'service_img_url', 'created_at', 'updated_at'
    ];

    // Name explanations:
    // service_name = The name of the service
    // service_description = Description of the service
    // service_img_url = possible image of the service
    // created_at = Timestamp of service creation
	// updated_at = Timestamp of service update
}
