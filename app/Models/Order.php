<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'project_id', 'provider_id', 'lat', 'long', 'cadastre', 'scan_time', 'sqr_meter', 'minutes_spent_in_air', 'events', 'gsd', 'max_height', 'service_type_id', 'total_price'
    ];

    // Name explanations:
    // gps_coordinates = GeoJSON format.
    // cadastre = land register, the number or id of the building/compound in question. All buildings are registered (https://gst.dk/matriklen/)
    // scan_time = The planned date and time for the drone scan to take place
    // sqr_meter = The horizontal area in square meters to be scanned (based on calculations from the map input)
    // minutes_spent_in_air = The amount of time the drone has been airborne. Calculated duration between when the first image was taken and the last image taken.
    // events = A log for events, accidents or other useful information that happened during the operation.
    // gsd = Short for Ground Sample Distance, determines the detail level of the images. The smaller a value, the higher detail level.
    // max_height = The maximum altitude the drone is allowed to fly. Depends on the local regulations.
    // service_type_id = Foreign key for model:ServiceType
	// total_price = The total cost of the order (the amount to charge the customer)

	protected $dates = ['scan_time'];

	/**
     * Get the project attached to the order.
     */
    public function project()
    {
        return $this->hasOne('App\Models\Project', 'id', 'project_id');
    }

	/**
     * Get the service attached to the order.
     */
    public function service()
    {
        return $this->hasOne('App\Models\OrderService', 'id', 'order_service_id');
    }
}
