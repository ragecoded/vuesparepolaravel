<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectPackage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'package_id', 'scans_remaining'
    ];

    // Name explanations:
    // package_id = Foreign key for model:Package
    // scans_remaning = The amount of scans remaining from the package. This value will decrease with 1 when a drone scan is performed. When 0 all scans has been used.
}
