<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubService extends Model
{
    protected $fillable = [
        'service_id', 'sub_service_name', 'sub_service_description', 'order_count', 'created_at', 'updated_at'
    ];

    // Name explanations:
    // service_id = The name of the parent service
    // sub_service_name = The name of the sub_service
    // sub_service_description = Description of the service
    // order_count = number of orders of said subservice
    // created_at = Timestamp of service creation
	// updated_at = Timestamp of service update
}
