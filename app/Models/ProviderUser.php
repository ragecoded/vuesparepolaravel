<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'provider_id', 'google_calendar_id', 'microsoft_calendar_id',  'price_per_hour', 'total_assigned_tasks', 'total_completed_scans',
		'total_sqr_meter_scanned', 'drone_certificate'
    ];

    // Name explanations:
	// user_id = References id on "users" table
	// provider_id References id on "providers" table
	// google_calendar_id = ID for the user's Google calendar
	// microsoft_calendar_id = ID for the user's Microsoft calendar
	// price_per_hour = The price per hour in DDK (no decimals)
	// total_assigned_tasks = Number of tasks assigned (for statistics)
	// total_completed_scans = Number of performed drone scans (for statistics)
	// total_sqr_meter_scanned = Total amount of square-meter scanned (for statistics)
	// drone_certificate = The user's drone certificate

	/**
     * Retrieves attached provider of the ProviderUser (in other words, returns the provider which is attached to the user).
     */
	public function provider()
	{
		return $this->belongsTo('App\Models\Provider', 'provider_id');
	}
}
