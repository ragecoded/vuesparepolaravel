<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Drone extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_id', 'drone_name', 'drone_model', 'drone_id', 'drone_register_number', 'drone_sensor', 'drone_visual_line_of_sight', 'control_station', 'description'
    ];

    // Name explanations:
    // provider_id = Foreign key for model:Provider
    // drone_name = Name of the drone
    // drone_model = Model of the drone
	// drone_id = The ID of the drone
	// drone_register_number = The regristration number for the drone
    // drone_sensor = Sensor mounted on the drone
	// drone_visual_line_of_sight = Visual line of sight, specified in meters
    // control_station = The ID of the control station used to control the drone
    // description = A description of the drone

	/**
     * Get the images for the drone.
     */
    public function images()
    {
        return $this->hasMany('App\Models\DroneImage');
    }

	/**
     * Get the provider attached to the drone.
     */
    public function provider()
    {
        return $this->hasOne('App\Models\Provider', 'id', 'provider_id');
    }
}
