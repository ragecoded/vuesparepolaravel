<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'project_name' => $this->project_name,
            'project_address' => $this->project_address,
            'project_house_number' => $this->project_house_number,
            'project_address' => $this->project_address,
            'project_postal_code' => $this->project_postal_code,
            'project_city' => $this->project_city,
            'project_country' => $this->project_country,
            'thumbnail_url' => $this->thumbnail_url,
            'completed' => $this->completed,
            'approved' => $this->approved,
            'is_owner' => $this->is_owner,
            'owner_name' => $this->owner_name,
            'owner_email' => $this->owner_email,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
        return parent::toArray($request);
    }
}
