<?php

namespace App\Http\Controllers\Api;

use App\Models\SubService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function subServiceOutput()
    {
        $subservices = SubService::All();
            return response()->json($subservices);
    }
    public function parentSubServiceOutput($pID)
    {
        $subservices = SubService::where('service_id', $pID)->get();
        return response()->json($subservices);
    }
}
