<?php

namespace App\Http\Controllers\Api;

use App\Models\ProjectOrder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class OrderController extends Controller
{
    public function store(Request $request)
    {
        
              
        $order = new ProjectOrder;
        $order->p_id = $request->p_id;
		$order->date = $request->date;
		$order->area = $request->area;
		$order->perimeter = $request->perimeter;
		$order->service_id = $request->service_id;
		$order->sub_service_id = $request->sub_service_id;
		$order->package = $request->package;
		$order->pname = $request->pname;
		$order->created_at = date("Y-m-d");;
		$order->save();
		
		return response()->json(array('success' => true), 200);
		

    }
}
