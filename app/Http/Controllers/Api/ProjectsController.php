<?php

namespace App\Http\Controllers\Api;

use App\Models\Project;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ProjectResource;

class ProjectsController extends Controller
{
    public function index()
    {
        return ProjectResource::collection(Project::paginate(9));
    }
    public function userProjects($id)
    {
        $project = Project::where('user_id', $id)->get();
        return response()->json($project);
    }
    public function projectDetails($uID, $pID)
    {
        $project = Project::where([['user_id', '=', $uID], ['id', '=', $pID]])->get();
        return response()->json($project);
    }
    public function store($projectData)
    {
        $project = new Project;

        $project->project_name = $projectData->project_name;
        $project->project_address = $projectData->project_address;
        $project->project_house_number = $projectData->project_house_number;
        $project->project_postal_code = $projectData->project_postal_code;
        $project->project_city = $projectData->project_city;
        $project->lat = $projectData->lat;
        $project->long = $projectData->long;
        $project->project_country = 'DK';
        $project->is_owner = $projectData->is_owner;
        $project->owner_name = $projectData->owner_name;
        $project->owner_email = $projectData->owner_email;
        $project->save();

        return response()->json(array('success' => true, 'last_insert_id' => $project->id), 200);
    }
}
